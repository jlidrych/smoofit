import numpy as np
import jax.numpy as jnp
from jax import lax
import jax.ops as jops

from .utils import *

class PaddedFunction(object):
    def __init__(self, fn, variables):
        self.fn = fn
        self.variables = variables
        self.proc_idxs = None
        self.chan_idxs = None
        self.idxs = None
    def notify_proc_idxs(self, idxs):
        self.proc_idxs = idxs
    def notify_chan_idxs(self, idxs):
        self.chan_idxs = idxs
    def build_idxs(self):
        if self.proc_idxs is not None and self.chan_idxs is None:
            self.idxs = self.proc_idxs
        elif self.proc_idxs is not None and self.chan_idxs is not None:
            self.idxs = (self.proc_idxs[:,jnp.newaxis], self.chan_idxs[jnp.newaxis,:])
        else:
            raise RuntimeError("not supported!")
    def __call__(self, x, factors):
        x = tuple(x[v.sub_idxs] for v in self.variables)
        return jops.index_mul(factors, self.idxs, self.fn(*x), True, True)

class LazySystematic(object):
    def expand_sub_procs(self, to_list, new_nominal):
        pass
    def merge_sub_vars(self, lazy_syst):
        pass
    def expand_sub_vars(self):
        pass
    @staticmethod
    def dummy(nominal, var, sub_vars, sub_procs):
        pass
    @staticmethod
    def merge_channels(left, right, right_nominal_yields, var, sub_vars, sub_procs):
        pass
    @staticmethod
    def build_concrete_from_list(lazy_syst_list, nominal, variables):
        pass

class Systematic(object):
    def __init__(self):
        self.var_idxs = None
    def build_idxs(self):
        self.var_idxs = jnp.concatenate([v.sub_idxs for v in self.variables], axis=-1)
    def __call__(self, x):
        pass

class LazyLogNormal(LazySystematic):
    def __init__(self, lnN, hist_shape, var, sub_vars=None, sub_procs=None, channel_idxs=None):
        super().__init__()
        self.hist_shape = hist_shape
        self.var = var
        if sub_vars is None:
            sub_vars = var.sub_names
        self.sub_vars = sub_vars
        self.sub_procs = sub_procs
        if isinstance(lnN, tuple) and len(lnN) == 2:
            self.up = self.adapt_shapes(lnN[0])
            self.down = self.adapt_shapes(lnN[1])
        else:
            self.up = self.adapt_shapes(lnN)
            self.down = self.up

    def adapt_shapes(self, lnN):
        lnN = to_np(lnN)
        var_dim = len(self.sub_vars)
        if self.sub_procs is None:
            n_sub_procs = self.hist_shape[0]
        else:
            n_sub_procs = len(self.sub_procs)
        target_shape = (var_dim, n_sub_procs, self.hist_shape[1])
        # need [sources]x[procs]x[bins]
        # explicit broadcasting to [bins] is needed for merging channels along that axis
        if len(self.hist_shape) == 2 and len(lnN.shape) == 1:
            if var_dim == 1 and lnN.shape[0] == 1:
                lnN = np.expand_dims(lnN, -1)
            elif lnN.shape[0] > 1:
                if var_dim > 1:
                    lnN = np.expand_dims(lnN, -1)
                else:
                    lnN = np.expand_dims(lnN, 0)
        lnN = np.expand_dims(lnN, -1) # for bins
        lnN = np.broadcast_to(lnN, target_shape)
        return lnN

    def expand_sub_procs(self, to_list, new_nominal):
        # fill in with up,down=1,1 for all the sub-processes not affected by this systematic
        self.hist_shape = new_nominal.shape
        def expand_array(arr, new_arr):
            for i,sub_proc in enumerate(to_list):
                if sub_proc in self.sub_procs:
                    j = self.sub_procs.index(sub_proc)
                    new_arr[:,i,:] = arr[:,j,:]
            return new_arr
        if to_list != self.sub_procs:
            # (#syst, #subproc, #bins)
            new_shape = (self.up.shape[0], len(to_list), self.up.shape[2])
            self.up = expand_array(self.up, np.ones(new_shape))
            self.down = expand_array(self.down, np.ones(new_shape))
            self.sub_procs = to_list

    def merge_sub_vars(self, lazy_syst):
        if lazy_syst.var != self.var:
            return self
        if lazy_syst is None:
            return self
        new_sub_vars = list(set(self.sub_vars + lazy_syst.sub_vars))
        new_sub_vars.sort(key=lambda sv: self.var.sub_names.index(sv))
        new_shape = (len(new_sub_vars), self.hist_shape[0], self.hist_shape[1])
        up = np.ones(new_shape)
        down = np.ones(new_shape)
        for i,sv in enumerate(new_sub_vars):
            if sv in self.sub_vars and sv in lazy_syst.sub_vars:
                raise ValueError("Sub-variable collision!")
            elif sv in self.sub_vars:
                j = self.sub_vars.index(sv)
                to_take_up = self.up[j,:,:]
                to_take_down = self.down[j,:,:]
            elif sv in lazy_syst.sub_vars:
                j = lazy_syst.sub_vars.index(sv)
                to_take_up = lazy_syst.up[j,:,:]
                to_take_down = lazy_syst.down[j,:,:]
            up[i,:,:] = to_take_up
            down[i,:,:] = to_take_down
        self.up = up
        self.down = down
        self.sub_vars = new_sub_vars

    def expand_sub_vars(self):
        new_shape = (len(self.var.sub_names), self.hist_shape[0], self.hist_shape[1])
        up = np.ones(new_shape)
        down = np.ones(new_shape)
        for i,sv in enumerate(self.var.sub_names):
            if sv in self.sub_vars:
                j = self.sub_vars.index(sv)
                to_take_up = self.up[j,:,:]
                to_take_down = self.down[j,:,:]
            up[i,:,:] = to_take_up
            down[i,:,:] = to_take_down
        self.up = up
        self.down = down
        self.sub_vars = self.var.sub_names

    @staticmethod
    def dummy(nominal, var, sub_vars, sub_procs, channel_idxs=None):
        return LazyLogNormal(1., nominal.shape, var, sub_vars, sub_procs, channel_idxs=channel_idxs)

    @staticmethod
    def merge_channels(left, right, right_nominal_yields, var, sub_vars, sub_procs):

        def merge(left, right):
            assert(left.var == right.var)
            assert(left.sub_procs == right.sub_procs)
            assert(left.sub_vars == right.sub_vars)
            left.hist_shape = (left.hist_shape[0], left.hist_shape[1] + right.hist_shape[1])
            left.up = np.concatenate((left.up, right.up), axis=-1)
            left.down = np.concatenate((left.down, right.down), axis=-1)
            return left

        if left is None and right is None:
            return LazyLogNormal.dummy(right_nominal_yields, var, sub_vars, sub_procs)
        elif left is None:
            return right
        elif right is None:
            return merge(left, LazyLogNormal.dummy(right_nominal_yields, var, sub_vars, sub_procs))
        else:
            return merge(left, right)

    @staticmethod
    def build_concrete_from_list(lazy_syst_list, nominal, variables):
        ups = []
        downs = []
        for var in variables:
            systs = [ s for s in lazy_syst_list if s.var == var ]
            up = np.concatenate([s.up for s in systs], axis=1)
            down = np.concatenate([s.down for s in systs], axis=1)
            ups.append(up)
            downs.append(down)
        up = np.concatenate(ups, axis=0)
        down = np.concatenate(downs, axis=0)
        total_var_dim = sum((var.dim for var in variables))
        concrete = AsymLogNormal(up, down, nominal.shape, variables)
        return concrete

class AsymLogNormal(Systematic):
    def __init__(self, up, down, hist_shape, variables):
        super().__init__()
        self.hist_shape = hist_shape
        self.up = up
        self.down = down
        self.x_shape = (self.up.shape[0], 1, 1)
        self.variables = variables
        self.var_idxs = None

    # HiggsCombine-like
    def __call__(self, x):
        if self.var_idxs is not None:
            x = x[self.var_idxs]
        x = jnp.reshape(x, self.x_shape)
        diff = self.up - self.down
        avg = 0.5 * (self.up + self.down)
        scale = jnp.where(x < -1.,
                        self.down,
                        jnp.where(x < 1.,
                                 (-0.25 * x**3 + 0.75 * x) * diff + avg,
                                 self.up))
        scale = jnp.where(scale > 0., scale, 1.)
        return jnp.exp(jnp.sum(x * jnp.log(scale), axis=0))

class LazyNormedSplineShapeUnc(LazySystematic):
    def __init__(self, nominal, up, down, var, sub_vars=None, sub_procs=None, channel_idxs=None):
        super().__init__()
        up = to_np(up)
        down = to_np(down)
        nominal = to_np(nominal)
        self.nominal = nominal
        if len(up.shape) == 1:
            up = up[np.newaxis,:]
        if len(down.shape) == 1:
            down = down[np.newaxis,:]
        # add dimension [k] or [i] if it's not present
        if len(up.shape) == len(nominal.shape):
            if nominal.shape[0] == 1:
                up = np.expand_dims(up, 1)
            else:
                up = np.expand_dims(up, 0)
        if len(down.shape) == len(nominal.shape):
            if nominal.shape[0] == 1:
                down = np.expand_dims(down, 1)
            else:
                down = np.expand_dims(down, 0)
        self.up = up
        self.down = down
        self.var = var
        if sub_vars is None:
            sub_vars = var.sub_names
        assert(len(sub_vars) == up.shape[0] == down.shape[0])
        self.sub_vars = sub_vars
        self.sub_procs = sub_procs
        self.channel_idxs = channel_idxs
        self._compute_integrals()

    def _compute_integrals(self):
        # careful here: if the lazy syst is already merged across several channels, the yield sums have to be computed
        # separately for each channel -> use the channel indices
        def _sum(arr):
            return np.broadcast_to(np.sum(arr, axis=-1, keepdims=True), arr.shape)
        def _integral(arr):
            if self.channel_idxs is None:
                return _sum(arr)
            else:
                return np.concatenate([ _sum(np.take(arr, idxs, axis=-1)) for idxs in self.channel_idxs ], axis=-1)
        self.nominal_int = _integral(self.nominal) # [i][j]
        self.up_int = _integral(self.up) # [k][i][j]
        self.down_int = _integral(self.down) # [k][i][j]

    def expand_sub_procs(self, to_list, new_nominal):
        # fill in with up,down=nominal yields for all the sub-processes not affected by this systematic
        self.nominal = new_nominal
        def expand_array(syst_arr, nominal, new_shape):
            ret = np.zeros(new_shape)
            for i,sub_proc in enumerate(to_list):
                if sub_proc in self.sub_procs:
                    j = self.sub_procs.index(sub_proc)
                    ret[:,i,:] = syst_arr[:,j,:]
                else:
                    ret[:,i,:] = nominal[i,:]
            return ret
        if to_list != self.sub_procs:
            # (#syst, #subproc, #bins)
            new_shape = (self.up.shape[0], len(to_list), self.up.shape[-1])
            self.up = expand_array(self.up, new_nominal, new_shape)
            self.down = expand_array(self.down, new_nominal, new_shape)
            self.sub_procs = to_list

    def merge_sub_vars(self, lazy_syst):
        if lazy_syst.var != self.var:
            return self
        if lazy_syst is None:
            return self
        new_sub_vars = list(set(self.sub_vars + lazy_syst.sub_vars))
        new_sub_vars.sort(key=lambda sv: self.var.sub_names.index(sv))
        new_shape = (len(new_sub_vars), self.up.shape[1], self.up.shape[2])
        up = np.zeros(new_shape)
        down = np.zeros(new_shape)
        for i,sv in enumerate(new_sub_vars):
            if sv in self.sub_vars and sv in lazy_syst.sub_vars:
                raise ValueError("Sub-variable collision!")
            elif sv in self.sub_vars:
                j = self.sub_vars.index(sv)
                to_take_up = self.up[j,:,:]
                to_take_down = self.down[j,:,:]
            elif sv in lazy_syst.sub_vars:
                j = lazy_syst.sub_vars.index(sv)
                to_take_up = lazy_syst.up[j,:,:]
                to_take_down = lazy_syst.down[j,:,:]
            up[i,:,:] = to_take_up
            down[i,:,:] = to_take_down
        self.up = up
        self.down = down
        self.sub_vars = new_sub_vars

    def expand_sub_vars(self):
        new_shape = (len(self.var.sub_names), self.up.shape[1], self.up.shape[2])
        up = np.zeros(new_shape)
        down = np.zeros(new_shape)
        for i,sv in enumerate(self.var.sub_names):
            if sv in self.sub_vars:
                j = self.sub_vars.index(sv)
                to_take_up = self.up[j,:,:]
                to_take_down = self.down[j,:,:]
            else:
                to_take_up = self.nominal
                to_take_down = self.nominal
            up[i,:,:] = to_take_up
            down[i,:,:] = to_take_down
        self.up = up
        self.down = down
        self.sub_vars = self.var.sub_names
        # inputs should not change anymore, so we can recompute the integrals for the channel before merging the channels 
        self._compute_integrals()

    @staticmethod
    def dummy(nominal, var, sub_vars, sub_procs, channel_idxs=None):
        return LazyNormedSplineShapeUnc(nominal, nominal, nominal, var, sub_vars, sub_procs, channel_idxs=channel_idxs)

    @staticmethod
    def merge_channels(left, right, right_nominal_yields, var, sub_vars, sub_procs):
        def merge(left, right):
            assert(left.sub_procs == right.sub_procs)
            assert(left.sub_vars == right.sub_vars)
            left.nominal = np.concatenate((left.nominal, right.nominal), axis=-1)
            left.up = np.concatenate((left.up, right.up), axis=-1)
            left.down = np.concatenate((left.down, right.down), axis=-1)
            left.nominal_int = np.concatenate((left.nominal_int, right.nominal_int), axis=-1)
            left.up_int = np.concatenate((left.up_int, right.up_int), axis=-1)
            left.down_int = np.concatenate((left.down_int, right.down_int), axis=-1)
            return left

        if left is None and right is None:
            return LazyNormedSplineShapeUnc.dummy(right_nominal_yields, var, sub_vars, sub_procs)
        elif left is None:
            return right
        elif right is None:
            return merge(left, LazyNormedSplineShapeUnc.dummy(right_nominal_yields, var, sub_vars, sub_procs))
        else:
            return merge(left, right)

    @staticmethod
    def build_concrete_from_list(lazy_syst_list, nominal, variables):
        ups = []
        downs = []
        up_ints = []
        down_ints = []
        nominal_int = None
        for var in variables:
            systs = [ s for s in lazy_syst_list if s.var == var ]
            # concatenate across processes
            up = np.concatenate([ s.up for s in systs ], axis=1)
            down = np.concatenate([ s.down for s in systs ], axis=1)
            up_int = np.concatenate([ s.up_int for s in systs ], axis=1)
            down_int = np.concatenate([ s.down_int for s in systs ], axis=1)
            ups.append(up)
            downs.append(down)
            up_ints.append(up_int)
            down_ints.append(down_int)
            if nominal_int is None:
                nominal_int = np.concatenate([ s.nominal_int for s in systs ], axis=0)
        # concatenate across sources
        up = np.concatenate(ups, axis=0)
        down = np.concatenate(downs, axis=0)
        up_int = np.concatenate(up_ints, axis=0)
        down_int = np.concatenate(down_ints, axis=0)
        total_var_dim = sum((var.dim for var in variables))
        return NormedSplineShapeUnc(nominal, up, down, variables, nominal_int, up_int, down_int)


class NormedSplineShapeUnc(Systematic):
    def __init__(self, nominal, up, down, variables, nominal_int=None, up_int=None, down_int=None):
        super().__init__()
        # nominal = H[i][j] where i=proc, j=bin
        # up,down = U/D[k][i][j] where k=syst source

        nominal_int = np.sum(nominal, axis=-1, keepdims=True) if nominal_int is None else nominal_int # [i][1]
        self.nominal_normed = nominal / np.where(nominal_int == 0., 1., nominal_int) # [i][j]
        up_int = np.sum(up, axis=-1, keepdims=True) if up_int is None else up_int # [k][i][1]
        self.up_normed = up / jnp.where(up_int == 0., 1., up_int) # [k][i][j]
        down_int = np.sum(down, axis=-1, keepdims=True) if down_int is None else down_int # [k][i][1]
        self.down_normed = down / np.where(down_int == 0., 1., down_int) # [k][i][j]
        up_norm = np.where(nominal_int == 0., 1., up_int / nominal_int) # [k][i][j]
        down_norm = np.where(down_int == 0., 1., nominal_int / down_int) # [k][i][j]

        self.norm_interp = AsymLogNormal(up_norm, down_norm, nominal.shape, variables)

        self.x_shape = (up.shape[0], 1, 1)
        self.variables = variables

    # HiggsCombine-like
    def normed_spline(self, x):
        x2 = x**2
        deriv1 = self.up_normed - self.down_normed
        deriv2 = self.up_normed + self.down_normed - 2. * self.nominal_normed
        return self.nominal_normed + 0.5 * x * deriv1 + 0.5 * (0.125 * x2 * (3.*x2**2 - 10.*x2 + 15.)) * deriv2
    def normed_lin_down(self, x):
        return self.down_normed - (x + 1.) * (self.down_normed - self.nominal_normed)
    def normed_lin_up(self, x):
        return self.up_normed + (x - 1.) * (self.up_normed - self.nominal_normed)

    def shape_interp(self, x):
        nominal_normed_forshape_nonzero = jnp.where(self.nominal_normed > 0., self.nominal_normed, 1.)
        scale = jnp.where(x < -1.,
                        self.normed_lin_down(x),
                        jnp.where(x <= 1.,
                                 self.normed_spline(x),
                                 self.normed_lin_up(x)))
        # divide by nominal normalized yields to have multiplicative factors
        return jnp.prod(scale / nominal_normed_forshape_nonzero, axis=0)

    def __call__(self, x):
        if self.var_idxs is not None:
            x = x[self.var_idxs]
        x = jnp.reshape(x, self.x_shape)
        return self.shape_interp(x) * self.norm_interp(x)

