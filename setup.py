import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="smoofit",
    version="0.0.1",
    author="Sébastien Wertz",
    author_email="sebastien.wertz@cern.ch",
    
    description="A jax-based package for auto-differentiable binned likelihood fits",
    long_description=long_description,
    long_description_content_type="text/markdown",

    url="https://smoofit.readthedocs.io/",

    packages=setuptools.find_packages(),
    
    classifiers=[
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Topic :: Scientific/Engineering :: Physics',
        'Topic :: Scientific/Engineering :: Statistics',
        'Topic :: Software Development :: Libraries :: Python Modules',

        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',

        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],

    python_requires='>=3.7',
    install_requires=[
        "numpy",
        "scipy",
        "jax",
        "jaxlib"
    ],
    extras_require={
        "examples": ["matplotlib"],
        "docs": ["Sphinx",
                 "sphinx-autodoc-typehints",
                 "sphinx-rtd-theme",
                 "nbsphinx",
                ],
        "tests": ["pytest", "pytest-cov"],
    },
)
