Overview
========


There are only four object types to understand for building models in smoofit:

- ``Variable``: These represent the parameters of the model (free POIs or nuisance parameters).
- ``Process``: As the name suggest, these objects represent the different processes to be considered (signals or backgrounds).
- ``ChannelContrib``: What do processes do? They *contribute* to the channels of the analysis. Channels represent any orthogonal event selections (data-taking eras, channels, signal/control regions, etc). A ``ChannelContrib`` object defines how a given process contributes to a given channel, including all relevant systematic uncertainties.
- ``Model``: The complete model holding all the variables and processes, which defines the likelihood function and is used to fit the data, estimate uncertainties etc.

Any likelihood function that can be written in the following way is supported:

.. math::
    L(\vec{D},\vec{B}|\vec{\mu}, \vec{\alpha}) = - \sum_{j=1}^{n} \text{LogPoi} \left( D_j \bigg\rvert \sum_{i=1}^p N_{ij}(\vec{\mu}, \vec{\alpha}) \right) + C(\vec{B}|\vec{\alpha}) + R(\vec{\mu},\vec{\alpha},\vec{D},\vec{B})

assuming one can write

.. math::
    N_{ij}(\vec{\mu}, \vec{\alpha}) = \prod_k f_{kij}(\vec{\mu}, \vec{\alpha}) N_{ij}^0

where

- :math:`\vec{\mu}` are POIs, :math:`\vec{\alpha}` are nuisance parameters
- :math:`N_{ij}^0` are the nominal predicted yields in bin :math:`j` for process :math:`i` (channels are just other bins!)
- :math:`D_j` are observed yields in bin :math:`j`
- :math:`f_{kij}` are arbitrary functions (which includes systematics uncertainties)
- :math:`\vec{B}` is a vector of "global observables", used for consistent frequentist treatment of nuisances (usually :math:`\vec{B}=\vec{0}`)
- :math:`C(\vec{B}|\vec{\alpha})` is the nuisance parameter constraint term, including arbitrary correlations (multi-dimensional Gaussian)
- :math:`R(\vec{\mu},\vec{\alpha},\vec{D},\vec{B})` is an arbitrary user-supplied function, e.g. for unfolding regularization (in which case it would only depend on :math:`\vec{\mu}`)

