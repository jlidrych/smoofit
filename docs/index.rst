.. smoofit documentation master file, created by
   sphinx-quickstart on Fri Dec 11 17:47:26 2020.

Smoofit: a package for smooth binned likelihood fits
====================================================

|docs| |gitlab-ci| |pycov|

Smoofit is a package for binned likelihood fits in high-energy physics.
It leverages the power of `JAX <https://jax.readthedocs.io/>`__ to quickly compute
the likelihood, even for large models featuring many bins, channels, processes or systematics,
and to analytically compute the gradient of the likelihood (as well as the
fit covariance matrix), leading to fast and stable fits.
In addition, the computations can be offloaded to GPUs, leading to significant
speed-ups when fitting large models.

It supports nearly arbitrary functional dependencies of the process yields
on the parameters of interests, enabling the implementation of
complex models for inference in the context of effective field theories,
or for unfolding differential cross sections with or without regularization.

Development
-----------

Smoofit is hosted on the `CERN gitlab <https://gitlab.cern.ch/swertz/smoofit/>`__.
This package is very much a beta version, still under active development: although the basic features
needed for template fits are in place, many are still missing: have a look at the
`list of open issues <https://gitlab.cern.ch/swertz/smoofit/-/issues>`__.
Also, there is no guarantee that the results are correct (it hasn't been validated
against other fitting tools yet), and there might be bugs lurking in the dark!
Suggestions, bug reports or contributions are therefore welcome!

.. toctree::
   :maxdepth: 4
   :caption: User guide

   installation
   overview

.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   notebooks/simple.ipynb

.. toctree::
   :maxdepth: 4
   :caption: API reference

   apiref

Index
=====

* :ref:`genindex`


.. |docs| image:: https://readthedocs.org/projects/smoofit/badge/?version=latest
    :alt: Documentation status
    :target: https://smoofit.readthedocs.io/en/latest/?badge=latest

.. |gitlab-ci| image:: https://gitlab.cern.ch/swertz/smoofit/badges/master/pipeline.svg
    :alt: Test status
    :target: https://gitlab.cern.ch/swertz/smoofit/-/commits/master

.. |pycov| image:: https://gitlab.cern.ch/swertz/smoofit/badges/master/coverage.svg
    :alt: Test coverage
    :target: https://gitlab.cern.ch/swertz/smoofit/-/commits/master

