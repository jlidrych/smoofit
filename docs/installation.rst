Installation
============

Using pip
---------

Only ``pip`` is supported at this time.

.. code:: bash

   python3 -m pip install git+https://gitlab.cern.ch/swertz/smoofit.git

If you plan to contribute to or modify Smoofit, you might prefer this method:

.. code:: bash

   python3 -m venv smoofit_venv
   source smoofit_venv/bin/activate
   git clone ssh://git@gitlab.cern.ch:7999/swertz/smoofit.git
   cd smoofit
   python3 -m pip install -e .


Smoofit on GPU
--------------

JAX, and hence Smoofit, will automatically use available GPUs provided you have:

* properly set up CUDA on your system,
* installed JAX for GPU, see instructions `here <https://github.com/google/jax#pip-installation>`__.

Building the documentation
--------------------------

Install additional requirements:

.. code:: bash

   cd smoofit/
   python3 -m pip install ".[docs]"

Build the documentation:

.. code:: bash

   cd docs/
   make html

The HTML documentation is then available under ``docs/_build/html/index.html``.

Running the test
----------------

Install additional requirements:

.. code:: bash

   cd smoofit/
   python3 -m pip install ".[tests]"

Run the tests:

.. code:: bash

   cd tests/
   pytest --cov=smoofit

