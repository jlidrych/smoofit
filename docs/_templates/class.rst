{{ fullname | escape | underline}}

.. currentmodule:: {{ module }}

.. autoclass:: {{ objname }}

   {% block methods %}
   {% if methods %}
   .. rubric:: {{ _('Methods') }}

   .. autosummary::
   {% for item in all_methods %}
		{%- if not item.startswith('_') or item in ['__init__',
                                                  '__call__',
                                                  ] %}
      	~{{ name }}.{{ item }}
		{% endif %}
   {%- endfor %}
   {% endif %}

   {% for item in all_methods %}
		{%- if not item.startswith('_') or item in ['__init__',
                                                  '__call__',
                                                  ] %}
   .. automethod:: {{ item }}
		{% endif %}
   {%- endfor %}
   {% endblock %}

   {% block attributes %}
   {% if attributes %}
   .. rubric:: {{ _('Attributes') }}

   .. autosummary::
   {% for item in attributes %}
      ~{{ name }}.{{ item }}
   {%- endfor %}
   {% endif %}
   {% endblock %}

