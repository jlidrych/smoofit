# Smoofit: a package for smooth binned likelihood fits

[![Documentation Status](https://readthedocs.org/projects/smoofit/badge/?version=latest)](https://smoofit.readthedocs.io/en/latest/?badge=latest)
[![pipeline status](https://gitlab.cern.ch/swertz/smoofit/badges/master/pipeline.svg)](https://gitlab.cern.ch/swertz/smoofit/-/commits/master) 
[![coverage report](https://gitlab.cern.ch/swertz/smoofit/badges/master/coverage.svg)](https://gitlab.cern.ch/swertz/smoofit/-/commits/master) 

Smoofit is a package for binned likelihood fits in high-energy physics.
It leverages the power of [JAX](https://jax.readthedocs.io/) to quickly compute
the likelihood, even for large models featuring many bins, channels, processes or systematics,
and to analytically compute the gradient of the likelihood as well as the
fit covariance matrix, leading to fast and stable fits.
In addition, the computations can be offloaded to GPUs, leading to significant
speed-ups when fitting large models.

It supports nearly arbitrary functional dependencies of the process yields
on the parameters of interests, enabling the implementation of
complex models for inference in the context of effective field theories,
or for unfolding differential cross sections with or without regularization.

## Documentation

Installation instructions, quickstart guide, tutorials and API reference
are available in the [online documentation](https://smoofit.readthedocs.io/).

## Development

This package is still under active development: although the basic features
for template fits are in place, many are still missing: have a look at the
[list of open issues](https://gitlab.cern.ch/swertz/smoofit/-/issues).
Also, there is no guarantee that the results are correct (it hasn't been validated
against other fitting tools yet), and there might be bugs lurking in the dark!
Suggestions, bug reports or contributions are therefore welcome!
