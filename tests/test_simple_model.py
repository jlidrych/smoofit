import pytest

import numpy as np
import jax.numpy as jnp

from smoofit.model import Variable, ChannelContrib, Process, Model

@pytest.fixture
def simple_signal_template():
    return np.linspace(10., 100., 10)

@pytest.fixture
def simple_signal_contrib(simple_signal_template):
    c = ChannelContrib("sr", simple_signal_template)
    return c

@pytest.fixture(params=[1., 2.])
def default_poi_value(request):
    return jnp.array([request.param])

@pytest.fixture(params=[0.005, 2.])
def alt_poi_value(request):
    return jnp.array([request.param])

@pytest.fixture
def simple_poi(default_poi_value):
    return Variable("test", default_poi_value[0], lower_bound=0.)

@pytest.fixture
def simple_signal_process(simple_signal_contrib, simple_poi): 
    p = Process("signal")
    p.add_contrib(simple_signal_contrib)
    p.scale_by(simple_poi)
    return p

@pytest.fixture
def simple_model(simple_signal_process):
    m = Model()
    m.add_proc(simple_signal_process)
    m.prepare()
    return m

def test_prepared(simple_model, simple_poi):
    assert(simple_model.prepared == True)
    assert(simple_model.n_var == 1)
    assert(simple_model.dim_var == 1)
    assert(simple_model.dim_nuisances == 0)
    assert(len(simple_model.var_sub_index(simple_poi)) == simple_poi.dim)

def test_default_values(simple_model, default_poi_value):
    vals = simple_model.values_from_dict()
    assert(vals == default_poi_value)

def test_injected_values(simple_model, simple_poi, alt_poi_value):
    vals = simple_model.values_from_dict({simple_poi: alt_poi_value[0]})
    assert(vals == alt_poi_value)
    with pytest.raises(ValueError):
        simple_model.values_from_dict({simple_poi: [0.]*2})

def test_glob(simple_model):
    assert(len(simple_model.default_glob()) == 0)

def test_obs(simple_model, simple_signal_template):
    concat = simple_model.concat_obs(simple_signal_template)
    assert(all(concat == jnp.array(simple_signal_template)))

    concat = simple_model.concat_obs({"sr": simple_signal_template})
    assert(all(concat == jnp.array(simple_signal_template)))

def test_pred(simple_model, simple_signal_template, alt_poi_value):
    pred = simple_model.pred(alt_poi_value)
    assert(all(pred == alt_poi_value * jnp.array(simple_signal_template)))

def test_asimov_fit(simple_model, simple_signal_template, default_poi_value):
    nObs = default_poi_value[0] * simple_signal_template
    best_fit = simple_model.fit(nObs)
    assert(best_fit.success)
    assert(np.all(np.isclose(best_fit.x, default_poi_value, rtol=1e-5)))

def test_nll_crossings(simple_model, simple_signal_template):
    nObs = simple_signal_template
    best_fit = simple_model.fit(nObs)
    root = simple_model.nll_crossings(np.array([1.]), best_fit, nObs)
    assert(root.converged)
    assert(np.isclose(root.x, 1.04325, rtol=1e-3))
    root = simple_model.nll_crossings(np.array([-1.]), best_fit, nObs)
    assert(root.converged)
    assert(np.isclose(root.x, 0.95796, rtol=1e-3))

def test_minos_intervals(simple_model, simple_signal_template, simple_poi):
    nObs = simple_signal_template
    best_fit = simple_model.fit(nObs)
    up,down,fit_up,fit_down = simple_model.minos_bounds(simple_poi, best_fit, nObs)
    assert(fit_up.success)
    assert(fit_down.success)
    assert(np.isclose(up, 1.04325, rtol=1e-3))
    assert(np.isclose(down, 0.95796, rtol=1e-3))

def test_sample_from_cov(simple_model, simple_signal_template, simple_poi):
    nObs = simple_signal_template
    best_fit = simple_model.fit(nObs)
    postfit_params = simple_model.sample_from_covariance(best_fit, 10)
    assert(postfit_params.shape == (10, simple_model.dim_var))
