#!/usr/bin/env python3

import time

import numpy as np
import jax.numpy as jnp

from smoofit.model import Variable,Process,Model

if __name__ == "__main__":
    proc = Process("proc", jnp.array([50., 1., 0.]))
    vproc = Process("vproc", jnp.vstack(([50., 1., 0.], [100., 1., 0.], [0., 0., 100.])))

    mu = Variable("mu", 1.)
    proc.scale_by(mu)
    vproc.scale_by(mu)

    vmu = Variable("vmu", [1., 1., 1.])
    vproc.scale_by(vmu)

    ## Symmetric lnN

    lnN_single = Variable("lnN_single", 0., nuisance=True)
    lnN_vec = Variable("lnN_vec", [0., 0., 0.], nuisance=True, sub_names=["1", "2", "3"])

    # Single lnN, same for all procs
    proc.add_lnN(lnN_single, 1.05)
    vproc.add_lnN(lnN_single, 1.05)

    # Single lnN, restricted to a sub-proc
    vproc.add_lnN(lnN_single, 1.05, subprocs=[0, 2])

    # Different lnN for each sub-procs
    vproc.add_lnN(lnN_single, [1.05, 1.1, 1.02])

    # Multiple lnNs, same for all procs
    proc.add_lnN(lnN_vec, [1.15, 1.2, 1.1])
    vproc.add_lnN(lnN_vec, [1.15, 1.2, 1.1])

    # Multiple lnNs, restricted to a sub-var
    proc.add_lnN(lnN_vec, 1.1, subvars=["1"])
    vproc.add_lnN(lnN_vec, 1.1, subvars=["2"])

    # Different lnN for each sub-procs
    vproc.add_lnN(lnN_vec, [[1.25, 1.3, 1.2], [1.4, 1.5, 1.3], [1.1, 1.2, 1.3]]) # source x proc

    # Different lnN for each sub-procs, restricted to sub-procs and sub-vars
    vproc.add_lnN(lnN_vec, [[1.25, 1.3], [1.1, 1.2]], subprocs=[1, 2], subvars=[0, 1]) # source x proc

    ## Asymmetric lnN

    asym_lnN_single = Variable("asym_lnN_single", 0., nuisance=True)
    asym_lnN_vec = Variable("asym_lnN_vec", [0., 0., 0.], nuisance=True)

    # Single lnN, same for all procs
    proc.add_lnN(asym_lnN_single, (1.05, 1.04))
    vproc.add_lnN(asym_lnN_single, (1.05, 1.04))

    # Single lnN, restricted to a sub-proc
    vproc.add_lnN(asym_lnN_single, (1.05, 1.04), subprocs=[0, 1])

    # Different lnN for each sub-procs
    vproc.add_lnN(asym_lnN_single, ([1.05, 1.1, 1.2], [1.04, 1.09, 1.1]))

    # Multiple lnNs, same for all procs
    proc.add_lnN(asym_lnN_vec, ([1.15, 1.2, 1.1], [1.14, 1.19, 1.2]))
    vproc.add_lnN(asym_lnN_vec, ([1.15, 1.2, 1.1], [1.14, 1.19, 1.2]))

    # Multiple lnNs, restricted to a sub-var
    proc.add_lnN(asym_lnN_vec, ([1.15, 1.2], [1.14, 1.19]), subvars=[1, 2])
    vproc.add_lnN(asym_lnN_vec, ([1.15, 1.2], [1.14, 1.19]), subvars=[1, 2])

    # Different lnN for each sub-procs
    vproc.add_lnN(asym_lnN_vec, ([[1.25, 1.3, 1.1], [1.4, 1.5, 1.2], [1.1, 1.2, 1.3]], [[1.24, 1.29, 1.1], [1.39, 1.49, 1.2], [1.3, 1.2, 1.1]])) # source x proc

    # Different lnN for each sub-procs, restricted to sub-procs and sub-vars
    vproc.add_lnN(asym_lnN_vec, ([1.25, 1.3], [1.2, 1.4]), subprocs=[0, 1], subvars=[1]) # source x proc

    ## Shape

    shape_single = Variable("shape_single", 0., nuisance=True)
    shape_vec = Variable("shape_vec", [0., 0., 0.], nuisance=True)

    # Single
    proc.add_shape_syst(shape_single, 1.2 * proc.yields, 0.8 * proc.yields)
    vproc.add_shape_syst(shape_single, 1.2 * vproc.yields, 0.8 * vproc.yields)

    # Single, restricted to subprocess
    vproc.add_shape_syst(shape_single, 1.1 * vproc.yields[0,:], 0.9 * vproc.yields[0,:], subprocs=[0])

    # Multiple
    proc.add_shape_syst(shape_vec, jnp.stack((1.2 * proc.yields, 1.1 * proc.yields, 1.3 * proc.yields), 0), jnp.stack((0.8 * proc.yields, 0.9 * proc.yields, 0.8 * proc.yields), 0))
    vproc.add_shape_syst(shape_vec, jnp.stack((1.2 * vproc.yields, 1.1 * vproc.yields, 1.2 * vproc.yields), 0), jnp.stack((0.8 * vproc.yields, 0.9 * vproc.yields, 0.7 * vproc.yields), 0))

    # Multiple, restricted to subprocess or subvar
    proc.add_shape_syst(shape_vec, 1.2 * proc.yields, 0.8 * proc.yields, subvars=[1])
    vproc.add_shape_syst(shape_vec, 1.2 * vproc.yields[1,:], 0.8 * vproc.yields[1,:], subprocs=[1], subvars=[0])

    model = Model()
    model.add_proc(proc)
    model.add_proc(vproc)

    start = time.perf_counter()
    model.enable_bblite()
    model.prepare()
    model.compile()
    print(f"Time to compile: {time.perf_counter() - start:.2f}s")
