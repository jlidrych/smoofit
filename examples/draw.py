#!/usr/bin/env python3

import numpy as np
import jax.numpy as jnp

import matplotlib.pyplot as plt

from smoofit.model import Variable,Process,Model

if __name__ == "__main__":
    nBkg = 1

    sig = Process("sig", jnp.array([50., 25., 5., 3., 2., 1., 1.]))
    bkgs = [ Process(f"bkg{i}", jnp.array([15., 25., 50., 25., 10., 5., 10.])) for i in range(nBkg) ]

    mu = Variable("mu", 1., lower_bound=0.)
    sig.scale_by(mu)

    lnN_both = Variable("lnN_both", 0., nuisance=True)
    sig.add_lnN(lnN_both, 1.05)
    for bkg in bkgs:
        bkg.add_lnN(lnN_both, 1.05)

    model = Model()
    model.add_proc(sig)
    for bkg in bkgs:
        model.add_proc(bkg)
    
    model.prepare()
    model.compile()

    values = model.values_from_dict({mu: 1.})
    obs = model.pred(model.values_from_dict({mu: 2.}))

    best_fit = model.fit(obs)
    mu_idx = mu.sub_idxs[0]
    best_mu = best_fit.x[mu_idx]
    up,down,up_minos_bf,__ = model.minos_bounds(mu, best_fit, obs)
    direction = np.zeros((len(best_fit.x,)))
    direction[mu_idx] = up-best_mu
    up_stat = model.nll_crossings(direction, best_fit, obs).x[mu_idx]
    down_stat = model.nll_crossings(-direction, best_fit, obs).x[mu_idx]

    x = np.linspace(1.7, 2.3, 50)
    y = np.linspace(-1.2, 1.2, 50)
    xv, yv = np.meshgrid(x, y)
    xyv = np.dstack((xv, yv))

    z_nll = jnp.apply_along_axis(lambda x: 2 * (model.nll(x, obs) - best_fit.fun), -1, xyv).reshape((len(x), len(y)))
    plt.contourf(xv, yv, z_nll, 50)
    plt.colorbar()
    for mu_prof in jnp.linspace(1.75, 2.26, 7):
        prof = model.profile({mu: mu_prof}, best_fit, obs)
        plt.plot([mu_prof, mu_prof], [y[0], y[-1]], ls=":", color='r')
        plt.plot([mu_prof], [prof.x[1]], 'or')
    plt.gca().set_ylabel("nuisance")
    plt.gca().set_xlabel("$\mu$")
    plt.savefig("profiling.pdf")
    plt.show()

    z_nll = jnp.apply_along_axis(lambda x: 2 * (model.nll(x, obs) - best_fit.fun), -1, xyv).reshape((len(x), len(y)))
    z = jnp.apply_along_axis(lambda x: -x[0], -1, xyv).reshape((len(x), len(y)))
    plt.contourf(xv, yv, z, 50)
    plt.colorbar()
    cs = plt.contourf(xv, yv, z_nll, [1., 1000.], colors='none', hatches=['/'])
    for collection in cs.collections:
        collection.set_edgecolor("red")
    plt.plot([x[0], x[-1]], [0., 0.], ls=":", color='r')
    plt.plot([up_stat], [0], 'or')
    plt.gca().set_ylabel("nuisance")
    plt.gca().set_xlabel("$\mu$")
    plt.savefig("crossings.pdf")
    plt.show()

    z_nll = jnp.apply_along_axis(lambda x: 2 * (model.nll(x, obs) - best_fit.fun), -1, xyv).reshape((len(x), len(y)))
    z = jnp.apply_along_axis(lambda x: -x[0], -1, xyv).reshape((len(x), len(y)))
    plt.contourf(xv, yv, z, 50)
    plt.colorbar()
    plt.contour(xv, yv, z_nll, [1.], colors='r')
    plt.plot([up, up], [y[0], y[-1]], ls=":", color='r')
    plt.plot([up], [up_minos_bf.x[1]], 'or')
    plt.gca().set_ylabel("nuisance")
    plt.gca().set_xlabel("$\mu$")
    plt.savefig("minos.pdf")
    plt.show()
