#!/usr/bin/env python3

import argparse
import os
import multiprocessing as mp

import numpy as np
from jax.interpreters import xla
import jax.profiler

import matplotlib.pyplot as plt

import time
import resource
import copy
import gc

from smoofit.model import Variable,Process,Model,ChannelContrib

def build_model(nBins, nBkg, nNuis, nPOI, nChans):
    sig = Process("sig", sub_procs=[f"sig{i}" for i in range(nPOI)])
    bkgs = [ Process(f"bkg{i}") for i in range(nBkg) ]
    nuisances = [ Variable(f"nuisance{i}", 0., nuisance=True) for i in range(nNuis) ]

    for c in range(nChans):
        s_contrib = ChannelContrib(f"chan{c}", np.vstack([np.linspace(0., 500., nBins) for i in range(nPOI)]), sub_procs=sig.sub_procs)
        for v in nuisances:
            # s_contrib.add_lnN(nuisances, (1.2, 0.8), sub_vars=[i])
            s_contrib.add_shape_syst(v, 1.2*s_contrib.yields, 0.8*s_contrib.yields)
        sig.add_contrib(s_contrib)

        for i,b in enumerate(bkgs):
            bkg = np.ones(nBins) * float(5000//nBkg)
            b_contrib = ChannelContrib(f"chan{c}", bkg+float(i))
            for v in nuisances:
                # b_contrib.add_lnN(v, (1.2, 0.8))
                b_contrib.add_shape_syst(v, 1.2*b_contrib.yields, 0.8*b_contrib.yields)
            b.add_contrib(b_contrib)

    mu = Variable(f"mu", [1.]*nPOI)
    sig.scale_by(mu)

    model = Model()
    model.add_proc(sig)
    for bkg in bkgs:
        model.add_proc(bkg)

    model.prepare()
    values = model.values_from_dict()
    return model,values

def get_rss():
    return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024

def measure_metrics(queue, default, scanned, n):
    xla._xla_callable.cache_clear()
    results = {}

    print(f"Doing {scanned} = {n}")
    thisScan = copy.deepcopy(default)
    thisScan[scanned] = n
    start = time.perf_counter()
    model,values = build_model(**thisScan)
    results["preparation_rss"] = get_rss()
    results["preparation"] = time.perf_counter() - start
    glob = model.default_glob()

    start = time.perf_counter()
    obs = model.pred(values).block_until_ready()
    results["first_pred"] = time.perf_counter() - start

    start = time.perf_counter()
    model.nll_fast(values, obs, glob).block_until_ready()
    results["first_nll"] = time.perf_counter() - start

    start = time.perf_counter()
    model.nll_grad(values, obs, glob).block_until_ready()
    results["first_grad"] = time.perf_counter() - start

    results["total_rss"] = get_rss()

    start = time.perf_counter()
    for i in range(nEval):
        model.pred(values).block_until_ready()
    results["pred"] = (time.perf_counter() - start)/nEval*1000

    start = time.perf_counter()
    for i in range(nEval):
        model.nll_fast(values, obs, glob).block_until_ready()
    results["nll"] = (time.perf_counter() - start)/nEval*1000

    start = time.perf_counter()
    for i in range(nEval):
        model.nll_grad(values, obs, glob).block_until_ready()
    results["grad"] = (time.perf_counter() - start)/nEval*1000

    # jax.profiler.save_device_memory_profile("memory.prof")

    queue.put(results)

def scan_and_plot(outDir, default, ranges, nEval, output=""):
    if output != "":
        output = "_" + output
    for scanned,thisRange in ranges.items():
        metrics = {}
        for m in ["preparation_rss", "total_rss", "preparation", "first_pred", "first_nll", "first_grad", "pred", "nll", "grad"]:
            metrics[m] = []
        for n in thisRange:
            q = mp.Queue()
            p = mp.Process(target=measure_metrics, args=(q, default, scanned, n))
            p.start()
            results = q.get()
            p.join()
            for m in metrics.keys():
                if results is None or m not in results:
                    print(f"Warning: could not measure {m} for {scanned} and {n}")
                    metrics[m].append(0)
                else:
                    metrics[m].append(results[m])
        # print(metrics)

        fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(13, 4))
        ax1.plot(thisRange, metrics["first_pred"], label="First prediction")
        ax1.plot(thisRange, metrics["first_nll"], label="First NLL")
        ax1.plot(thisRange, metrics["first_grad"], label="First gradient")
        ax1.plot(thisRange, metrics["preparation"], label="Model preparation")
        ax1.set(xlabel=scanned, ylabel="s")
        ax2.plot(thisRange, metrics["pred"], label=f"Prediction")
        ax2.plot(thisRange, metrics["nll"], label=f"NLL")
        ax2.plot(thisRange, metrics["grad"], label=f"Gradient")
        ax2.set(xlabel=scanned, ylabel="ms")
        ax3.plot(thisRange, metrics["total_rss"], label=f"Total RAM")
        ax3.plot(thisRange, metrics["preparation_rss"], label=f"RAM for preparation")
        ax3.set(xlabel=scanned, ylabel="MB")
        for ax in fig.get_axes():
            ax.legend()
        forTitle = copy.deepcopy(default)
        forTitle.pop(scanned)
        fig.suptitle(", ".join([ f"{i}={k}" for i,k in forTitle.items() ]))
        fig.tight_layout()
        fig.savefig(os.path.join(outDir, scanned + output + ".pdf"))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", help="Output folder")
    parser.add_argument("--large", action="store_true", help="Also go through larger model")
    args = parser.parse_args()

    if not os.path.isdir(args.output):
        os.makedirs(args.output, exist_ok=True)

    nEval = 1000

    default = { "nBins": 50, "nBkg": 5, "nNuis": 5, "nPOI": 1, "nChans": 1 }
    ranges = {
        "nBkg":  np.arange(5, 106, 20),
        "nNuis": np.arange(5, 106, 20),
        "nBins": np.arange(10, 3011, 500),
        "nPOI": np.concatenate(([1], np.arange(5, 56, 10))),
        "nChans": np.concatenate(([1], np.arange(5, 56, 10))),
    }
    # ranges = { "nBkg": np.array([50]) }
    scan_and_plot(args.output, default, ranges, nEval)

    default = { "nBins": 100, "nBkg": 10, "nNuis": 50, "nPOI": 1, "nChans": 5 }
    ranges = {
        "nBkg":  np.arange(5, 106, 20),
        "nNuis": np.arange(5, 506, 100),
        "nBins": np.arange(10, 1511, 500),
        "nPOI": np.arange(5, 106, 20),
        "nChans": np.arange(5, 56, 10),
    }
    # ranges = { "nBkg": np.array([50]) }
    if args.large:
        scan_and_plot(args.output, default, ranges, nEval, "large")
